/*
 * Ignas.c
 *
 * Created: 2019-03-13 12:16:14
 * Author : 20164361
 */ 
#define F_CPU 1600000
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
volatile uint8_t tot_overflow;

void init_pwm()
{
	TCCR0|=(1<<WGM00)|(1<<WGM01)  
	|(1<<COM01)                  
	|(1<<CS00);        
	DDRB|=(1<<PB3);
}

int main()
{
	uint8_t i=0;     
	init_pwm();      
	while(1)
	{
		
		for(i=0;i<255;i++)
		{
			
			OCR0=i;
		
			_delay_ms(100);
		}
		
		for(i=255;i>0;i--)
		{
		
			OCR0=i;
		
			_delay_ms(10);
		}
	}

	return (0);
}