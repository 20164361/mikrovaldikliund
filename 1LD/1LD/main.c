/*
 * ignasveleckis1ld.c
 *
 * Created: 2019-02-13 12:15:50
 * Author : 20164361
 
#define F_CPU 16000000

#include <avr/io.h>
#include <util/delay.h>


int main(void)
{
	int a[2]={15,255};
	int i = 0;
    while (1) 
    {
		DDRA=255;
		PORTA=a[i];
		if (i<2){
			i=i+1;
		}
		else{
			i=0;
		}
		_delay_ms(1000);
			
    }
}
*/
#define F_CPU 16000000

#include <avr/io.h>
#include <util/delay.h>

int main(void)
{
	DDRA=255;
	int a[12]={1,2,4,8,16,64,128,64,16,8,4,2};
	int i = 0;
	while (1)
	{
		
		PORTA=a[i];
		
		if(i<11) 
		{
			i=i+1;
		}
		
		else 
		{
			i=0;
		}
		
		 _delay_ms(50);
	}
	
		
}